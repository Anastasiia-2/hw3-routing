import React from "react";
import { MdChair } from "react-icons/md";
import styles from "./Logo.module.scss";

export default function Logo() {
  return (
    <a href="/" className={styles.logo}>
      <span>{<MdChair />}</span> Furniking
    </a>
  );
}
