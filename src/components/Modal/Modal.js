import PropTypes from "prop-types";
import { MdClose } from "react-icons/md";
import Button from "../Button/Button";
import styles from "./Modal.module.scss";

function Modal({ header, closeButton, handleBtnClick, text, actions }) {
  return (
    <div
      data-wrapper
      className={styles.backdrop}
      onClick={e => e.target.dataset.wrapper && handleBtnClick()}
    >
      <div className={styles.modal}>
        <div className={styles.containerHeader}>
          <h2 className={styles.title}>{header}</h2>

          {closeButton && (
            <Button
              text={<MdClose />}
              handleBtnClick={handleBtnClick}
              className="closeButton"
            />
          )}
        </div>
        <p className={styles.text}>{text}</p>
        {actions}
      </div>
    </div>
  );
}

Modal.propTypes = {
  header: PropTypes.string,
  closeButton: PropTypes.bool,
  handleBtnClick: PropTypes.func,
  text: PropTypes.string,
  actions: PropTypes.any,
};

Modal.defaultProps = {
  header: PropTypes.string,
  closeButton: PropTypes.bool,
  handleBtnClick: PropTypes.func,
  text: PropTypes.string,
  actions: PropTypes.any,
};

export default Modal;
