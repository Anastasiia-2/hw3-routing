import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import styles from "./Header.module.scss";

import Logo from "../Logo/Logo";
import AdditionalInformaition from "../AdditionalInformaition/AdditionalInformaition";
import PageNavigation from "../PageNavigation/PageNavigation";

export default function Header({ cartItems, items }) {
  const [cartAmount, setCartAmount] = useState(0);
  const [favAmount, setFavAmount] = useState(0);

  useEffect(() => {
    setCartAmount(cartItems.reduce((acc, item) => (acc += item.count), 0));
  }, [cartItems]);

  useEffect(
    () =>
      setFavAmount(
        items.reduce((acc, item) => {
          if (item.isFavorited) {
            return (acc += 1);
          }
          return acc;
        }, 0)
      ),
    [items]
  );

  return (
    <section className={styles.pageHeader}>
      <div className={styles.greetings}>
        <p>Welcome to our online shop </p>
      </div>
      <header className={styles.headerContainer}>
        <Logo />
        <PageNavigation />

        <AdditionalInformaition cartCount={cartAmount} favСount={favAmount} />
      </header>
    </section>
  );
}

Header.propTypes = {
  cartItems: PropTypes.array.isRequired,
  items: PropTypes.array.isRequired,
};
