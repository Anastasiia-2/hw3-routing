import React from "react";
import PropTypes from "prop-types";
import styles from "./ItemsContainer.module.scss";
import Item from "../Item/Item";

export default function ItemsContainer({
  items,
  setCurrentItem,
  toggleModal,
  setItems,
  handleDeleteBtnClick,
}) {
  return (
    <ul className={styles.list}>
      {items.length
        ? items.map(item => {
            return (
              <Item
                handleDeleteBtnClick={handleDeleteBtnClick}
                key={item.id}
                item={item}
                toggleModal={toggleModal}
                setCurrentItem={setCurrentItem}
                setItems={setItems}
                isDelete={item.isInCart}
              />
            );
          })
        : ""}
    </ul>
  );
}

ItemsContainer.propTypes = {
  items: PropTypes.arrayOf(PropTypes.object).isRequired,
  toggleModal: PropTypes.func.isRequired,
  setCurrentItem: PropTypes.func.isRequired,
  setItems: PropTypes.func.isRequired,
};
