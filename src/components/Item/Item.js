import React from "react";
import { MdClose } from "react-icons/md";
import PropTypes from "prop-types";
import { AiOutlineHeart, AiFillHeart } from "react-icons/ai";
import { BsCartPlus } from "react-icons/bs";

import styles from "./Item.module.scss";
import Button from "../Button/Button";

export default function Item(props) {
  const { item, toggleModal, setCurrentItem, setItems, isDelete } = props;

  const { src, price, name, id, isFavorited } = item;

  const handleAddToCurrentItemClick = () => {
    setCurrentItem(item);
    toggleModal();
  };

  const handleFavBtnClick = e => {
    setItems(prev => {
      const newItems = [...prev];

      // const index = newItems.indexOf(item);
      const index = newItems.findIndex(el => el.id === id);
      newItems[index].isFavorited = !isFavorited;

      localStorage.setItem("favor", JSON.stringify(newItems));

      return newItems;
    });
  };

  return id ? (
    <li className={styles.item}>
      <div className={styles.itemImg}>
        <img src={src} alt={name} />
        <Button
          text={<BsCartPlus size="24" />}
          handleBtnClick={handleAddToCurrentItemClick}
          classBtn={styles.itemAddToCartBtn}
        />
        <Button
          text={
            item.isFavorited ? (
              <AiFillHeart size="24" className={styles.icon} />
            ) : (
              <AiOutlineHeart size="24" className={styles.icon} />
            )
          }
          handleBtnClick={handleFavBtnClick}
          classBtn={item.isFavorited ? styles.favorite : styles.itemAddToFavBtn}
        />
      </div>
      <div className={styles.textWrapper}>
        <p className={styles.itemTitle}>{name}</p>
        <p className={styles.itemPrice}> {price}</p>
      </div>
      {isDelete ? (
        <Button
          text={<MdClose />}
          handleBtnClick={handleAddToCurrentItemClick}
          className={styles.closeButton}
          classBtn={styles.closeButton}
        />
      ) : (
        ""
      )}
    </li>
  ) : (
    ""
  );
}

Item.propTypes = {
  item: PropTypes.object.isRequired,
  toggleModal: PropTypes.func.isRequired,
  setCurrentItem: PropTypes.func.isRequired,
  setItems: PropTypes.func,
  handleBtnClick: PropTypes.func,
};

Item.defaultProps = {
  handleBtnClick: () => {},
};
