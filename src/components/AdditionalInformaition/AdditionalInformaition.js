import React from "react";
import PropTypes from "prop-types";
import { AiOutlineHeart } from "react-icons/ai";
import { BsCart } from "react-icons/bs";
import styles from "./Additional.module.scss";

export default function AdditionalInformaition({ cartCount, favСount }) {
  return (
    <div className={styles.additionalWrapper}>
      <p className={styles.actions}>
        <BsCart size="24" />
        <p className={styles.actionsCount}>{cartCount}</p>
      </p>

      <p className={styles.actions}>
        <AiOutlineHeart size="24" />{" "}
        <p className={styles.actionsCount}>{favСount}</p>
      </p>
    </div>
  );
}

AdditionalInformaition.propTypes = {
  cartCount: PropTypes.number.isRequired,
  favСount: PropTypes.number.isRequired,
};
