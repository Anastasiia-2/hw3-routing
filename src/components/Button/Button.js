import PropTypes from "prop-types";
import styles from "./Button.module.scss";

function Button({ text, handleBtnClick, backgroundColor, classBtn }) {
  return (
    <>
      <button
        className={classBtn ? classBtn : styles.button}
        onClick={handleBtnClick}
        type="button"
      >
        {text}
      </button>
    </>
  );
}

Button.propTypes = {
  text: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  handleBtnClick: PropTypes.func.isRequired,
  backgroundColor: PropTypes.string,
  classBtn: PropTypes.string,
};

Button.defaultProps = {
  backgroundColor: "#555555",
  classBtn: "",
};

export default Button;
