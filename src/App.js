import { useState, useEffect } from "react";
import AppRoutes from "./AppRoutes";
import Modal from "./components/Modal/Modal";
import Button from "./components/Button/Button";
import Header from "./components/Header/Header";
import "./App.css";

function App() {
  const [items, setItems] = useState([]);
  const [currentItem, setCurrentItem] = useState({});
  const [cartItems, setCartItems] = useState([]);
  const [isShowModal, setIsShowModal] = useState(false);
  const [isShowCartModal, setIsShowCartModal] = useState(false);

  const addToCartList = () => {
    const { src, price, name, id, isFavorited } = currentItem;
    setCartItems(prevState => {
      const newCartItemsState = [...prevState];
      const itemCart = newCartItemsState.find(item => item.id === id);
      if (itemCart) {
        itemCart.isInCart = true;
        itemCart.count++;

        localStorage.setItem("cart", JSON.stringify(newCartItemsState));
        return newCartItemsState;
      } else {
        localStorage.setItem(
          "cart",
          JSON.stringify([
            {
              src,
              price,
              name,
              id,
              count: 1,
              isFavorited,
              isInCart: true,
            },
            ...prevState,
          ])
        );
        return [
          { src, price, name, id, count: 1, isFavorited, isInCart: true },
          ...prevState,
        ];
      }
    });
    toggleModal();
  };

  const deleteFromCartList = () => {
    setCartItems(prev => {
      const newCartItemsState = [...prev];
      const index = newCartItemsState.indexOf(currentItem);
      if (index !== -1) {
        newCartItemsState.splice(index, 1);
      }
      localStorage.setItem("cart", JSON.stringify(newCartItemsState));
      return newCartItemsState;
    });

    toggleCartModal();
  };

  const toggleModal = () => {
    setIsShowModal(prev => !prev);
  };

  const toggleCartModal = () => {
    setIsShowCartModal(prev => !prev);
  };

  useEffect(() => {
    const lsFavItems = localStorage.getItem("favor");

    if (lsFavItems) {
      setItems(JSON.parse(lsFavItems));
    } else {
      (async () => {
        try {
          const data = await fetch("./itemsList.json").then(res => res.json());
          setItems(data);
        } catch (error) {
          console.log(error);
        }
      })();
    }

    const lsCartValue = localStorage.getItem("cart");
    if (lsCartValue) {
      setCartItems(JSON.parse(lsCartValue));
    } else {
      setCartItems([]);
    }
  }, []);

  return (
    <div className="wrapper">
      <Header cartItems={cartItems} items={items} />
      <div className="App">
        <AppRoutes
          title="Our products"
          items={items}
          setItems={setItems}
          setCurrentItem={setCurrentItem}
          toggleModal={toggleModal}
          toggleCartModal={toggleCartModal}
          cartItems={cartItems}
          setCartItems={setCartItems}
        />

        {isShowModal && (
          <Modal
            header="Add to cart"
            closeButton={true}
            handleBtnClick={toggleModal}
            text="Do you want to add this product to your cart?"
            actions={
              <>
                <Button text="Yes" handleBtnClick={addToCartList} />
                <Button text="No" handleBtnClick={toggleModal} />
              </>
            }
          />
        )}

        {isShowCartModal && (
          <Modal
            header="Add to cart"
            closeButton={true}
            handleBtnClick={toggleCartModal}
            text="Do you want to delete this product from your cart?"
            actions={
              <>
                <Button text="Yes" handleBtnClick={deleteFromCartList} />
                <Button text="No" handleBtnClick={toggleModal} />
              </>
            }
          />
        )}
      </div>
    </div>
  );
}

export default App;
