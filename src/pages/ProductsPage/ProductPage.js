import React from "react";
import PropTypes from "prop-types";
import ItemsContainer from "../../components/ItemsContainer/ItemsContainer";
import styles from "./ProductPage.module.scss";

export default function ProductPage({
  title,
  items,
  setCurrentItem,
  toggleModal,
  setItems,
}) {
  return (
    <section>
      <h2 className={styles.title}>{title}</h2>
      <ItemsContainer
        items={items}
        setCurrentItem={setCurrentItem}
        toggleModal={toggleModal}
        setItems={setItems}
      />
    </section>
  );
}
ProductPage.propTypes = {
  title: PropTypes.string,
  items: PropTypes.arrayOf(PropTypes.object).isRequired,
  setItems: PropTypes.func.isRequired,
  setCurrentItem: PropTypes.func.isRequired,
  toggleModal: PropTypes.func.isRequired,
};

ProductPage.defaultProps = {
  title: "",
};
