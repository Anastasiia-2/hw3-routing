import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import ItemsContainer from "../../components/ItemsContainer/ItemsContainer";

export default function FavoritePage({
  items,
  toggleModal,
  setCurrentItem,
  setItems,
}) {
  const [newItems, setNewItems] = useState([]);

  useEffect(() => {
    setNewItems(items.filter(item => item.isFavorited));
  }, [items]);

  return newItems.length ? (
    <ItemsContainer
      items={newItems}
      setCurrentItem={setCurrentItem}
      toggleModal={toggleModal}
      setItems={setItems}
    />
  ) : (
    "You have not yet added any product to your favorites."
  );
}

FavoritePage.propTypes = {
  items: PropTypes.arrayOf(PropTypes.object).isRequired,
  toggleModal: PropTypes.func.isRequired,
  setCurrentItem: PropTypes.func.isRequired,
  setItems: PropTypes.func.isRequired,
  handleBtnClick: PropTypes.func,
};

FavoritePage.defaultProps = {
  handleBtnClick: () => {},
};
