import React, { useEffect } from "react";
import PropTypes from "prop-types";
import ItemsContainer from "../../components/ItemsContainer/ItemsContainer";

export default function CartPage({
  items,
  setCartItems,
  cartItems,
  setCurrentItem,
  toggleModal,
  setItems,
  handleDeleteBtnClick,
}) {
  useEffect(() => {
    const lsCartList = localStorage.getItem("cart") ?? [];
    const filtredItems = JSON.parse(lsCartList).filter(item => item.isInCart);

    setCartItems(filtredItems);
    localStorage.setItem("cart", JSON.stringify(filtredItems));
  }, [items, setCartItems]);

  return cartItems.length ? (
    <ItemsContainer
      items={cartItems}
      setCurrentItem={setCurrentItem}
      toggleModal={toggleModal}
      setItems={setItems}
      handleDeleteBtnClick={handleDeleteBtnClick}
    />
  ) : (
    "You have not added anything to your cart yet..."
  );
}

CartPage.propTypes = {
  items: PropTypes.arrayOf(PropTypes.object).isRequired,
  setCartItems: PropTypes.func.isRequired,
  cartItems: PropTypes.arrayOf(PropTypes.object).isRequired,
  setCurrentItem: PropTypes.func.isRequired,
  toggleModal: PropTypes.func.isRequired,
  setItems: PropTypes.func.isRequired,
  handleDeleteBtnClick: PropTypes.func,
};

CartPage.defaultProps = {
  handleDeleteBtnClick: () => {},
};
