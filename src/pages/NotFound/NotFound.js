import React from "react";

export default function NotFound() {
  return (
    <div
      style={{
        marginTop: "30px",
      }}
    >
      <p>The page is not found...</p>
      <div>
        <img
          src="https://aubankaitis.files.wordpress.com/2014/04/search_600.jpg"
          alt="not found"
          width={150}
          style={{
            display: "block",
            marginLeft: "auto",
            marginRight: "auto",
          }}
        />
      </div>
    </div>
  );
}
