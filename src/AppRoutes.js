import React from "react";
import { Routes, Route } from "react-router-dom";
import PropTypes from "prop-types";
import ProductPage from "./pages/ProductsPage/ProductPage";
import CartPage from "./pages/CartPage/CartPage";
import FavoritePage from "./pages/FavoritePage/FavoritePage";
import NotFound from "./pages/NotFound/NotFound";

export default function AppRoutes({
  title,
  items,
  setItems,
  setCurrentItem,
  toggleModal,
  toggleCartModal,
  setCartItems,
  cartItems,
  handleDeleteBtnClick,
}) {
  return (
    <Routes>
      <Route
        path="/"
        element={
          <ProductPage
            title={title}
            items={items}
            setCurrentItem={setCurrentItem}
            toggleModal={toggleModal}
            setItems={setItems}
          />
        }
      />
      <Route
        path="/cart"
        element={
          <CartPage
            items={items}
            setCartItems={setCartItems}
            cartItems={cartItems}
            setCurrentItem={setCurrentItem}
            toggleModal={toggleCartModal}
            toggleCartModal={toggleCartModal}
            setItems={setItems}
            handleDeleteBtnClick={handleDeleteBtnClick}
          />
        }
      />
      <Route
        path="/favorite"
        element={
          <FavoritePage
            items={items}
            toggleModal={toggleModal}
            setCurrentItem={setCurrentItem}
            setItems={setItems}
          />
        }
      />
      <Route path="*" element={<NotFound />} />
    </Routes>
  );
}

AppRoutes.propTypes = {
  title: PropTypes.string,
  items: PropTypes.arrayOf(PropTypes.object).isRequired,
  setItems: PropTypes.func.isRequired,
  setCurrentItem: PropTypes.func.isRequired,
  toggleModal: PropTypes.func.isRequired,
  toggleCartModal: PropTypes.func.isRequired,
  setCartItems: PropTypes.func.isRequired,
  cartItems: PropTypes.arrayOf(PropTypes.object).isRequired,
  handleDeleteBtnClick: PropTypes.func,
};

AppRoutes.defaultProps = {
  title: "",
  handleDeleteBtnClick: () => {},
};
